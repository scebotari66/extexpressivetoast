Ext.define('Expressive.toasts.overrides.Toast',{
	override: 'Ext.window.Toast',
	align: 'br',
	paddingX: 5,
	paddingY: 5,
	width: 210,
	slideInDuration: 500,

	/**
	 * @cfg {"error"/"info"/"success"/"warning"}
	 * Specifies the type of the toast.
	 *
	 * Possible values:
	 *
	 *  - error
	 *  - info
	 *  - success
	 *  - warning
	 */
	type: null,

	initComponent: function() {
		var me = this,
			type = me.type;

		if (type) {
			me.closable = true;
			me.cls = 'c-extoast c-extoast--' + type;
			switch (type) {
				case 'error':
					me.iconCls = 'fa fa-times-circle';
					break;
				case 'info':
					me.iconCls = 'fa fa-info-circle';
					break;
				case 'success':
					me.iconCls = 'fa fa-check-circle';
					break;
				case 'warning':
					me.iconCls = 'fa fa-exclamation-circle';
					break;
			}
		}

		me.callParent();
	}
});
